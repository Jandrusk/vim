syntax on
filetype indent plugin on

set t_Co=256
set background=dark
syntax on
colorscheme badwolf
set nu
set ts=2
map <F7> :tabn<CR>
map <F8> :tabp<CR>
map <C-Tab> gt
map <C-S-Tab> gT
inoremap jk <ESC>

nnoremap th  :tabfirst<CR>
nnoremap tj  :tabnext<CR>
nnoremap tk  :tabprev<CR>
nnoremap tl  :tablast<CR>
nnoremap tt  :tabedit<Space>
nnoremap tn  :tabnext<Space>
nnoremap tm  :tabm<Space>
nnoremap td  :tabclose<CR>
" Alternatively use
" "nnoremap th :tabnext<CR>
" "nnoremap tl :tabprev<CR>
" "nnoremap tn :tabnew<CR>
"



